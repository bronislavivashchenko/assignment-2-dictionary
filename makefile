ASM=nasm
ASMFLAGS=-f elf64
LD=ld
PY=python3

all: main

%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

main.o: main.asm colon.inc words.inc lib.inc dict.inc
lib.o: lib.asm
dict.o: dict.asm 

main: main.o lib.o dict.o
	$(LD) -o main $^

clean:
	rm -f *.o

test:
	$(PY) test.py 
