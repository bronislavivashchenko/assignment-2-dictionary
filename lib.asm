global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global print_error
global read_line

section .text
%define NEWLINE_CHAR 0xA
%define ASCII_ZERO "0"
%define MINUS_SYMBOL "-"
%define SPACE_CHAR 0x20
%define TAB_CHAR 0x9

 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    mov  rax, rdi
  .counter:
    cmp  byte [rax], 0
    je   .end
    inc  rax
    jmp  .counter
  .end:
    sub  rax, rdi
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rdi
    
    mov  rdx, rax
    mov  rsi, rdi
    mov  rax, 1
    mov  rdi, 1
    syscall
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stderr
print_error:
    push rdi
    call string_length
    pop rdi
    
    mov  rdx, rax
    mov  rsi, rdi
    mov  rax, 1
    mov  rdi, 2
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, 1
    mov rdi, 1
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, NEWLINE_CHAR
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov rcx, 10
    mov r8, rsp
	
	dec rsp
	mov [rsp], byte 0
    
  .loop:
    xor rdx, rdx
    div rcx
    
    add dl, ASCII_ZERO
	dec rsp
	mov [rsp], dl
    
    test rax, rax
    jnz .loop
  .end:
    
    mov rdi, rsp
	push r8
	call print_string
	
	pop rsp
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
	cmp rdi, 0
	jnl .positive
	
  .negative:
	push rdi
	
	mov rdi, MINUS_SYMBOL
	call print_char
	
	pop rdi
	neg rdi
	
  .positive:
	call print_uint
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
	mov rax, 1
	
  .loop:
	mov dl, byte[rdi]	
	cmp dl, byte[rsi]
	jnz .not_equal
	
	test dl, dl
	jz .end
	
	inc rdi
	inc rsi
	jmp .loop
	
  .not_equal:
	mov rax, 0
	
  .end:
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	dec rsp
	mov byte[rsp], 0
	
	xor rax, rax
	mov rdi, 0
	mov rsi, rsp
	mov rdx, 1
    syscall
	
	mov al, [rsp]
	inc rsp
	
	ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
	push rdi
	
	test rsi, rsi
	jz .end_fail
	
  .skip_spaces_loop:
	push rdi
	push rsi
	call read_char
	pop rsi
	pop rdi
	
	test rax, rax
	jz .end_fail
	
	cmp rax, SPACE_CHAR
	je .skip_spaces_loop
	cmp rax, TAB_CHAR
	je .skip_spaces_loop
	cmp rax, NEWLINE_CHAR
	je .skip_spaces_loop
	
	jmp .process_char
  
  .read_word_loop:
	push rdi
	push rsi
	call read_char
	pop rsi
	pop rdi
	
	test rax, rax
	jz .end_success
	
	cmp rax, SPACE_CHAR
	je .end_success
	cmp rax, TAB_CHAR
	je .end_success
	cmp rax, NEWLINE_CHAR
	je .end_success

  .process_char:
	cmp rsi, 1
	jle .end_fail
	
	mov byte[rdi], al
	inc rdi
	dec rsi
	jmp .read_word_loop
  
  .end_success:
	mov byte[rdi], 0
	
	pop rax
	mov rdx, rdi
	sub rdx, rax
    ret
  
  .end_fail:
	pop rax
	xor rax, rax
	xor rdx, rdx
    ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
	xor rcx, rcx
	xor rdx, rdx
	
  .loop:
	mov dl, byte[rdi + rcx]
	sub dl, ASCII_ZERO
	
	cmp dl, 0
	js .end
	cmp dl, 10
	jns .end
	
	imul rax, 10
	add rax, rdx
	inc rcx
	
	jmp .loop
	
  .end:
	mov rdx, rcx
	ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    mov dl, byte[rdi]
	cmp dl, MINUS_SYMBOL
	je .negative
	
  .positive:
	call parse_uint
    ret 

  .negative:
	inc rdi
	call parse_uint
	neg rax
	inc rdx
	ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rdi
	push rsi
	push rdx
	call string_length
	pop rdx
	pop rsi
	pop rdi
	
	cmp rdx, rax
	jle .error
	
	xor rax, rax
	
  .loop:
	mov dl, byte[rdi]
	mov byte[rsi], dl
	inc rdi
	inc rsi
	inc rax
	
	cmp dl, 0
	jz .end
	
	jmp .loop
	
  .end:
	ret
    
  .error:
	xor rax, rax
	ret

read_line:
	push r12
	push r13
	push rdi

	mov r12, rdi
	mov r13, rsi
	
	test rsi, rsi
	jz .end_fail
  
  .loop:
	call read_char
	
	test rax, rax
	jng .end_success
	
	cmp rax, NEWLINE_CHAR
	je .end_success

	cmp r13, 1
	jle .end_fail
	
	mov byte[r12], al
	inc r12
	dec r13
	jmp .loop
  
  .end_success:
	mov byte[r12], 0
	
	pop rax
	mov rdx, r12
	sub rdx, rax
	pop r13
	pop r12
    ret
  
  .end_fail:
	pop rax
	xor rax, rax
	xor rdx, rdx
	pop r13
	pop r12
    ret
