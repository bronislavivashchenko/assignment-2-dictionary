%define list_head 0
%define key_offset 8
%define value_offset 16

%macro colon 2
	%ifstr %1
        %ifid %2
			%%key: db %1, 0
			%2:
			dq list_head
			dq %%key
			%define list_head %2
		%else
            %error "Argument 2 should be a string"
        %endif
    %else
        %error "Argument 1 should be an id"
    %endif
%endmacro
