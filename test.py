import subprocess

cases = [
    {'input': 'First', 'output': 'First element', 'error': ''},
    {'input': 'Second', 'output': 'Second element', 'error': ''},
    {'input': 'Third', 'output': 'Third element', 'error': ''},
    {'input': 'Fourth', 'output': '', 'error': 'The element is not found'},
    {'input': 'Test', 'output': '', 'error': 'The element is not found'},
    {'input': 'ahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahahah', 'output': '', 'error': 'Buffer overflow'}
]

def run(input):
    process = subprocess.Popen(['./main'], 
                               stdin=subprocess.PIPE, 
                               stdout=subprocess.PIPE, 
                               stderr=subprocess.PIPE, 
                               universal_newlines=True)
    stdout, stderr = process.communicate(input)
    return stdout, stderr

def test(input, output, error):
    stdout, stderr = run(input)
    stdout = stdout.strip()
    stderr = stderr.strip()
    
    assert stdout == output and stderr == error,\
        f"Failed test \"{input}\".\nExpected stdout \"{output}\" and stderr \"{error}\", but got \"{stdout}\" and \"{stderr}\""

if __name__ == '__main__':
    for case in cases:
        test(**case)
    print("Tests passed!")
