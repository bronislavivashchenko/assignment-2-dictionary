%include "lib.inc"
%include "words.inc"
%include "dict.inc"

%define BUF_SIZE 256
%define NEWLINE_CHAR 0xA
%define PNTSIZE 8

section .bss
input_buf: resb BUF_SIZE

section .rodata
not_found_message: db "The element is not found", NEWLINE_CHAR, 0
buffer_overflow_message: db "Buffer overflow", NEWLINE_CHAR, 0

section .text
global _start
_start:
	mov rdi, input_buf
	mov rsi, BUF_SIZE
	call read_line
	
	test rax, rax
	jz .buffer_overflow
	
	mov rdi, rax
	mov rsi, list_head
	call find_word
	
	test rax, rax
	jz .not_found
	
	lea rdi, [rax + value_offset]
	call print_string
	call print_newline
	
	jmp .exit
	
  .buffer_overflow:
	mov rdi, buffer_overflow_message
	call print_error
	jmp .exit
	
  .not_found:
	mov rdi, not_found_message
	call print_error
	
  .exit:
	mov rdi, 0
	call exit
