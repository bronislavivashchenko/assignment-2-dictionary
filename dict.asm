global find_word

%include "lib.inc"
%include "colon.inc"

section .text
find_word:
  .loop:
  	test rsi, rsi
	jz .not_found

	push rdi
	push rsi
	
	mov rsi, [rsi + key_offset]
	call string_equals
	
	pop rsi
	pop rdi
	
	test rax, rax
	jnz .found
	
	mov rsi, [rsi]
	jmp .loop
  .found:
	mov rax, rsi
	ret

  .not_found:
	xor rax, rax
	ret
